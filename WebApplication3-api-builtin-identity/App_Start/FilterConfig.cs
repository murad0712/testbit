﻿using System.Web;
using System.Web.Mvc;

namespace WebApplication3_api_builtin_identity
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
